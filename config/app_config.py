#This config file is used to centralized all the configs that the project needs
#It is useful in production environments using environmental variables to determine
#paths, connections address and others

import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    pass

class ProductionConfig(Config):
    UPLOAD_FOLDER = 'data/training'
    SAVED_MODELS_PATH = 'ml_models/saved_models'
    ALLOWED_EXTENSIONS = set(['parquet'])

class DevelopmentConfig(Config):
    UPLOAD_FOLDER = 'data/training'
    SAVED_MODELS_PATH = 'ml_models/saved_models'
    ALLOWED_EXTENSIONS = set(['parquet'])


