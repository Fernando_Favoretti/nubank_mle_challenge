## Nubank Machine Learning Engineer Challenge
#### Fernando Favoretti

---

##
For this challenge, I produced a simple 
Flask based web service that allows to put a machine learning model into a production
environment. This server runs with support of Green Unicorn what is a python WSGI HTTP Server
, Gunicorn is fast, stable and commonly-used part of web app deployments that's powered some of the largest Python-powered web applications in the world, such as Instagram. Also, Gunicorn is the recommended choice for new Python web applications today.


## Service Endpoints

1. **General Health check** : `nbk/mle/health`

Simple health check, return if the system is up:

  ```
  
  
        [GET]
        Returns: 200, {"STATUS": "UP"}

   ```

2. **Basic predict**:  `/nbk/mle/predict`  

 Is the base predict endpoint, this use the most recent model to
        generate a prediction based on a input:
        
 ```
 
       Input:
       Type: [POST] - application/json

        
       Body:
       {
        "id": "8db4206f-8878-174d-7a23-dd2c4f4ef5a0",
        "score_3": 480.0,
        "score_4": 105.2,
        "score_5": 0.8514,
        "score_6": 94.2,
        "income": 50000
       }
  ```
      
       Response: 200,
       
       {
        "model_version": 1, #The version of the model used in predction
        "id": "8db4206f-8878-174d-7a23-dd2c4f4ef5a0", 
        "prediction": 0.1495367804089391 
       }


3. **Retraining**:  `/nbk/mle/retrain`  

 The endpoint that user will use to retrain the model. When accessed the api
 will generate a new version of machine learning model based on the new training
 file inputed by the user
 
 ```
 
 
       Input:
       Type: [POST] 
       Args: ['file'] - multipart/formdata with the File to upload (the field name need to be 'file')
       * Only parquet files will be received
  ```
  
  
       Response: 200,
       
       {
        "message": "Successful Retrain",
        "auc": 0.5944811727507904, #auc of the new model
        "new_created_version": 2 #new version createad
        }
        
4. **Prediction with versioning**:  `/nbk/mle/predict/v/<version_number>`  

    When the user wants to use a diferent version in the prediction
    
 ```
 
 
       Input:
       Type: [POST] - application/json
       
       Args: <version_number> - An integer with the number of the version
        
       Body:
       {
        "id": "8db4206f-8878-174d-7a23-dd2c4f4ef5a0",
        "score_3": 480.0,
        "score_4": 105.2,
        "score_5": 0.8514,
        "score_6": 94.2,
        "income": 50000
       }
  ```
  
  
       example of call :  http://ip:port/nbk/mle/predict/v/2
       Response: 200,
       
        {
        "model_version": "2",
        "id": "8db4206f-8878-174d-7a23-dd2c4f4ef5a0",
        "prediction": 0.1495367804089391
        }
        
5. **Check Active Versions** : `nbk/mle/versions`

    Check active versions, when they were created and how to access then.
    
  ```
  
  
        [GET]
        Returns: 200,
         {
            "version": 2,
            "predict_url": "/nbk/mle/predict/v/2",
            "last_modified": "Thu Feb 1 09:51:47 2018"
         },
         {
            "version": 1,
            "predict_url": "/nbk/mle/predict/v/1",
            "last_modified": "Thu Feb 1 09:51:47 2018"
         }

   ```

#### How this work

The way the service works has been fully designed for productive environments.
The service starts with a Singleton instance of the most recent model saved in memory,
for the use in the `/nbk/mle/predict` endpoint. This provide fast and easy predictions

When the system receive a new file for retrain, it creates a new version of the model
and dispose it as the new Singleton instance.

For example: With only one version a user can get predictions using:
    `/nbk/mle/predict` to access the model already instantiated in memory or `/nbk/mle/predict/v/1` to get the same prediction
    
After retrained, the system create a new model version (in this case, version 2) the previously version only can be used by user with the `/nbk/mle/predict/v/1` because the
new Instance of model in memory reach by `/nbk/mle/predict` is now the version 2.


### Prerequisites
```
>> pip install -r requirements.txt
```

### Running the server
To run the code, in the root directory of the application (/nubank_mle_challenge):

```
>>  export PYTHONPATH='.'
>>  gunicorn -k gevent --workers *<numworkers>* --worker-connections=*<num>* --reload -b <ip_to_bind>:<port> core.app

```

**numworkers** - The number of worker processes for handling requests.

By the documentation of green unicorn is better to set like (num_cores*2)+1


**worker-connections** - The maximum number of simultaneous clients.

Tested with 2000


## Stress Test

Build with [Siege](https://www.joedog.org/siege-home/)

The tests were made on a Linux server with 8 cores and 8gb RAM
> Test 1:
```
20 concurrent users Send a POST request simultaneously 
without interval in /nbk/mle/predict  by two minutes

```
    Results:
    ** SIEGE 3.0.8
    ** Preparing 20 concurrent users for battle.
    The server is now under siege...
    Lifting the server siege..      done.
    
    Transactions:		       13700 hits
    Availability:		      100.00 %
    Elapsed time:		      119.18 secs
    Data transferred:	        1.32 MB
    Response time:		        0.17 secs
    Transaction rate:	      114.95 trans/sec
    Throughput:		        0.01 MB/sec
    Concurrency:		       19.98
    Successful transactions:       13700
    Failed transactions:	           0
    Longest transaction:	        0.34
    Shortest transaction:	        0.07

> Test 2:
```
Test: 20 concurrent users Send a POST request simultaneously 
without interval in /nbk/mle/predict/v/2 (version not instantiated in memory)  by two minutes

```
    Results:
    ** SIEGE 3.0.8
    ** Preparing 20 concurrent users for battle.
    The server is now under siege...
    Lifting the server siege...      done.
    
    Transactions:		       13100 hits
    Availability:		      100.00 %
    Elapsed time:		      119.70 secs
    Data transferred:	        1.29 MB
    Response time:		        0.18 secs
    Transaction rate:	      109.44 trans/sec
    Throughput:		        0.01 MB/sec
    Concurrency:		       19.98
    Successful transactions:       13100
    Failed transactions:	           0
    Longest transaction:	        0.88
    Shortest transaction:	        0.08


As we can see there is a little difference in the tests results using the current
version and the not instantiated on. This difference can be bigger depending on the
server computational power.

However, this solution was made for light running and it can be used in much more simple
machines (with 1 core and less than 1gb RAM) and keep giving a great serving


## Future

This application can be tunned in prodution with some help of infrastructure team:
we can create a streamming service using tools like [Kafka](https://kafka.apache.org/) or [Fluentd](https://www.fluentd.org/) to stream logs like
errors, predictions auc, number of prediction per sec and others to a graphical platform like
[kibana](https://www.elastic.co/products/kibana) to continuously monitor the service with dashboards.
 
Also we can use a tool like [new relic](https://newrelic.com/) to monitor the service in terms of performance of response and stress 


## Built With

* [Python 3.5.4](https://www.python.org/downloads/release/python-354/)
* [Flask](http://flask.pocoo.org/) - python micro framework
* [Green Unicorn](http://gunicorn.org/) - Python WSGI HTTP Server for UNIX


## Authors

* @fernandofavoretti

