'''
Based on: https://github.com/chembl/chembl_webresource_client/blob/master/chembl_webresource_client/singleton.py
'''
class Singleton:
    """
    A non-thread-safe helper class to ease implementing singletons.
    This should be used as a decorator -- not a metaclass -- to the
    class that should be a singleton.

    The decorated class can define one `__init__` function that
    takes only the `self` argument. Also, the decorated class cannot be
    inherited from. Other than that, there are no restrictions that apply
    to the decorated class.

    To get the singleton instance, use the `Instance` method. Trying
    to use `__call__` will result in a `TypeError` being raised.

    """

    def __init__(self, decorated):
        self._decorated = decorated

    def Instance(self):
        """
        Returns the singleton instance. Upon its first call, it creates a
        new instance of the decorated class and calls its `__init__` method.
        On all subsequent calls, the already created instance is returned.

        """
        try:
            return self._instance
        except AttributeError:
            self._instance = self._decorated()
            return self._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `Instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)


@Singleton
class Last_Model_Class:
    last_model = None
    model = None
    version = None

    def __init__(self):
        self.last_model, self.version = self.read_last_model()

    def read_last_model(self):
        '''
        Read the last version of the model
        :return: the model loaded with joblib and the version.
        '''
        from joblib import load
        from ml_models import model_functions
        saved_models_path = 'ml_models/saved_models/'
        version_paths = saved_models_path
        last_version = model_functions.get_last_version(version_paths)
        last_model = 'classifier_v_{}.nbk'.format(last_version)
        clf = load(version_paths+last_model)
        return clf,last_model

    def read_model(self, version=1):
        '''
        Use joblib to load the model according with the version specified by the user
        :param version:
        :return: the version of the model
        '''
        from joblib import load
        saved_models_path = 'ml_models/saved_models/'
        version_paths = saved_models_path
        model = 'classifier_v_{}.nbk'.format(version)
        clf = load(version_paths + model)
        return clf

    def get_last_model(self):
        '''
        return the las model in instance
        :return:
        '''
        return self.last_model

