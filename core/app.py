from flask import Flask, Response, request
from flask_cors import CORS
from werkzeug.utils import secure_filename
import os
from core.model_singleton import Last_Model_Class
#Do not remove the import bellow
from ml_models.saved_models import reload_log

#starting application
application = Flask(__name__)
CORS(application)

#Get conf of environment
application.config.from_object('config.app_config.DevelopmentConfig')

#Start with the last model in instance
clf = Last_Model_Class.Instance()


def allowed_file(filename):
    '''
    helper to receive only allowed files during update
    :param filename:
    :return: Boolean: True if the uploaded file can be accept by the service
    '''
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in application.config.get('ALLOWED_EXTENSIONS')



@application.route('/nbk/mle/health', methods=['GET'])
def health():
    '''
    :return: Simple health check, return 200 if the system is up
    '''
    import json
    health = {}
    health['STATUS'] = "UP"
    return json.dumps(health), 200


@application.route('/nbk/mle/predict', methods=['POST'])
def make_pred():
    '''
    Default predict function, always look up to the newest trained model
    :return: predict based on the newest instance model
    '''
    import json
    from ml_models import model_functions
    content = request.get_json(force=True)
    r = model_functions.make_predictions(content)
    return Response(json.dumps(r), status=200, mimetype='application/json')



@application.route('/nbk/mle/predict/v/<version>', methods=['POST'])
def make_pred_versioned(version):
    '''
    Predict function for older versions
    :param version:
    :return: predict based on version of the user's choice
    '''
    import json
    from ml_models import model_functions
    content = request.get_json(force=True)
    r = model_functions.make_predictions(content, version=version)
    return Response(json.dumps(r), status=200, mimetype='application/json')


@application.route('/nbk/mle/retrain', methods=['POST'])
def update_training():
    '''
    Receive a new train file to retrain our model and create a new classifier
    :return:
    '''
    import time
    from datetime import datetime
    from ml_models import model
    import json

    ts = time.time()
    if 'file' not in request.files:
        r = {}
        r["message"] = 'No file has been uploaded'
        return Response(json.dumps(r), status=500, mimetype='application/json')

    #get the file sent
    file = request.files['file']
    if file and allowed_file(file.filename):
        #make sure the file name is secure to store
        filename = secure_filename(file.filename)

        #save the file with the actual time stamp
        path = os.path.join(application.config.get('UPLOAD_FOLDER'), '{}_'.format(ts)+filename)
        file.save(path)

        #retrain the model
        version,auc = model.train(path)

        #write to log (just to force gunicorn to reload)
        f = open('ml_models/saved_models/reload_log.py', 'a')
        f.write('#new model added on - {}\n'.format(datetime.now()))  # python will convert \n to os.linesep
        f.close()
        r = {}

        #response
        r["message"] = 'Successful Retrain'
        r["auc"] = auc
        r["new_created_version"] = version
        return Response(json.dumps(r), status=200, mimetype='application/json')
    else:
        r = {}
        r["message"] = 'File not allowed'
        return Response(json.dumps(r), status=200, mimetype='application/json')


@application.route('/nbk/mle/versions', methods=['GET'])
def get_versions():
    from ml_models import model_functions
    import json
    r = model_functions.get_versions()
    return Response(json.dumps(r), status=200, mimetype='application/json')



if __name__ == '__main__':
    '''
    Start the application
    '''
    application.run(host='0.0.0.0', port=80, threaded=True)
