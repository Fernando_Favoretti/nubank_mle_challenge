def make_predictions(data, version = None):
    '''
    Make the predictions
    :param data: the json data of post
    :param version: the version of the model, none for the latest one
    :return:
    '''
    import pandas as pd
    from core.model_singleton import Last_Model_Class
    #Read the post data
    r = {}
    r["id"] = data["id"]

    #put the data on a dataframe
    df = pd.DataFrame.from_records(data, index=[0])
    df = df[["score_3", "score_4", "score_5", "score_6"]]

    #fillna if needed, like validation set on the model
    df["score_3"] = df["score_3"].fillna(455)

    #Get last version of the model if it is not defined
    #else get the specific one that the user wants
    clf_instance = Last_Model_Class.Instance()
    if version is None:
        clf = clf_instance.get_last_model()
        classifier, last_version = clf_instance.read_last_model()
    else:
        try:
            clf = clf_instance.read_model(version=version)
        except:
            return {"Messagge": "Version does not exists"}

    #make de predictions and return the probs
    probs = clf.predict_proba(df)
    r["prediction"] = probs[0][1]
    r["model_version"] = (int(last_version.split(".nbk")[0][-1])) if version is None else version
    return r


def get_last_version(path):
    '''
    List and sort the versions
    :param path:
    :return: The latest version
    '''
    import glob
    all_versions = glob.glob(path + "/*.nbk")
    list_versions = [0]
    for version in all_versions:
        list_versions.append(int(version.split(".nbk")[0][-1]))
    return sorted(list_versions)[-1]


def get_model_version(version):
    '''
    Get the full path of the saved model with a version
    :param version:
    :return:
    '''
    from core import app
    saved_models_path = app.application.config.get("SAVED_MODELS_PATH", 'saved_models')
    return saved_models_path+'/classifier_v_{}.nbk'.format(version)


def get_versions():
    '''
    Allow to user check all the available versions
    :return: th version name with the url and the creation date
    '''
    import glob
    import os
    from core import app
    from datetime import datetime
    #get the path from the configs
    saved_models_path = app.application.config.get("SAVED_MODELS_PATH", 'saved_models')
    all_versions =  glob.glob(saved_models_path + "/*.nbk")
    #create a list with the version info
    list_dicts_versions = []
    for version in all_versions:
        dict_versions = {}
        #get the version of file
        dict_versions["version"] = (int(version.split(".nbk")[0][-1]))
        #get the last modified date
        mod_timestamp = datetime.strftime(datetime.fromtimestamp(os.stat(saved_models_path).st_mtime), '%c')
        dict_versions["last_modified"] = mod_timestamp
        #show the url to use
        dict_versions["predict_url"] = '/nbk/mle/predict/v/{}'.format(dict_versions["version"])
        list_dicts_versions.append(dict_versions)

    return list_dicts_versions
